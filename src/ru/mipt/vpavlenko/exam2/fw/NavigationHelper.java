package ru.mipt.vpavlenko.exam2.fw;

/**
 * Created by alexanderg on 02.04.14.
 */
public class NavigationHelper extends HelperBase {

    public NavigationHelper(AppManager manager) {
        super(manager);
    }

    public NavigationHelper mainPage() {
        driver.manage().deleteAllCookies();
        driver.get(manager.baseURL);
        return this;
    }
}
