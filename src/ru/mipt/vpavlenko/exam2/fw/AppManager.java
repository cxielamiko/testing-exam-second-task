package ru.mipt.vpavlenko.exam2.fw;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by alexandergechis on 02.04.14.
 */
public class AppManager {

    WebDriver driver;
    public String baseURL;
    private HelperBase sumHelper;
    private NavigationHelper navigationHelper;

    public AppManager() {
        driver = new FirefoxDriver();
        this.baseURL = "http://provereno.ru/";
        driver.get(baseURL);
    }

    public void stop() {
        driver.close();
        driver.quit();
    }

    public HelperBase getSumHelper() {
        if (sumHelper == null) {
            sumHelper = new ProverenoHelper(this);
        }
        return sumHelper;
    }

    public NavigationHelper navigateTo() {
        if (navigationHelper == null) {
            navigationHelper = new NavigationHelper(this);
        }
        return navigationHelper;
    }

}
