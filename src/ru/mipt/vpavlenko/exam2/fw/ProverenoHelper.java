package ru.mipt.vpavlenko.exam2.fw;

import org.openqa.selenium.By;

/**
 * Created by alexandergechis on 02.04.14. Modified by vitalypavlenko on
 * 03.06.14.
 */

public class ProverenoHelper extends HelperBase {

	public ProverenoHelper(AppManager manager) {
		super(manager);
	}

	public static void main(String[] args) {
		new ProverenoHelper(new AppManager()).doEverything();
	}

	public void doEverything() {
		manager.navigateTo().mainPage();

		click(By.cssSelector("a[href=\"#tab_secondary\"]"));

		type(By.id("address"), "А");

		waitAndGet(By.cssSelector("#ui-id-1 > li:nth-child(3) > a")).click();

		waitAndGet(By.cssSelector("#ui-id-2 > li:nth-child(3) > a")).click();

		waitAndGet(By.cssSelector("button.go")).click();

		System.out
				.println("Стоимость за квадратный метр: "
						+ waitAndGet(
								By.cssSelector(".vindex-block > div:first-child > div:nth-child(2)"))
								.getText());

		System.out
				.println("Комфортность: "
						+ waitAndGet(
								By.cssSelector(".apart-rating > div:nth-child(2) > span:first-child"))
								.getText());
	}

}
