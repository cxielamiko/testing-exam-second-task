package ru.mipt.vpavlenko.exam2.fw;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by alexanderg on 02.04.14.
 * Modified by vitalypavlenko on 03.06.14.
 */
public class HelperBase {

	protected AppManager manager;
	protected WebDriver driver;
	public boolean acceptNextAlert = true;

	public HelperBase(AppManager manager) {
		this.manager = manager;
		this.driver = manager.driver;
	}

	public void type(By elementLocator, String inputText) {
		if (inputText != null) {
			WebElement element = driver.findElement(elementLocator);
			element.clear();
			element.sendKeys(inputText);
		}
	}

	public void click(By elementLocator) {
		driver.findElement(elementLocator).click();
	}

	protected WebElement waitAndGet(final By by) {
		return (new WebDriverWait(driver, 10)).until(ExpectedConditions
				.visibilityOfElementLocated(by));
	}

}
